const puppeteer = require('puppeteer');
const moment = require('moment');
const player = require('play-sound')(opts = {})
const command_line_args = require('command-line-args');
const get_config = require('./config');

const optionDefinitions = [
    { name: 'work', alias: 'w', type: Boolean, defaultValue: false},
    { name: 'hanako', alias: 'a', type: Boolean, defaultValue: false}
];
const options = command_line_args(optionDefinitions);
const config = get_config(options);

async function wait_navigation_after_action(page, action, selector, value){
    const promise = page.waitForNavigation({waitUntil: 'networkidle2'});
    switch(action){
        case 'click':
            page.click(selector);
            break;
        case 'select':
            page.select(selector, value);
            break;
        default:
            break;
    }
    await promise;
}

async function get_page(endpoint){
    if(endpoint===null){
        let browser = null;
        if(process.platform === 'linux'){
            browser = await puppeteer.launch({
                args: [ '--no-sandbox', '--disable-setuid-sandbox' ]
            });
        }else{
            browser = await puppeteer.launch({headless: false});
        }
        return browser.newPage();
    }else{
        const browser = await puppeteer.connect({browserWSEndpoint: endpoint});
        const pages = await browser.pages();
        return new Promise((resolve, reject) => {resolve(pages[0])});
    }
}

function get_comps(station_status){
    const comps = station_status.split('\n');
    if(comps.length === 3){
        const name = comps[0];
        const num_cycles = comps[2].substr(0, comps[2].length-1);
        return {name, num_cycles};
    }
    return null;
}
async function print_stations(page, stations){
    const res= await page.$$eval('div.pc_view a.port_list_btn_inner', elems => {
        const texts = [];
        elems.forEach(elem=>texts.push(elem.innerText));
        return texts;
    });
    res.forEach(text=>{
        if(stations.filter(station=> text.startsWith(station)).length > 0){
            const comps = get_comps(text);
            if(comps !== null){
                console.log(`${comps.name} => ${comps.num_cycles}`);
            }else{
                console.error(`Got unexpected string ${text}`);
            }
        }
    })
}

async function check_for_station(page, stations){
    const handle = await page.evaluateHandle((get_comps, stations) => {
        eval(get_comps);
        const page_links = document.querySelectorAll('div.pc_view a.port_list_btn_inner');
        const station_map = {};
        page_links.forEach(link=>{
            const text = link.innerText;
            const comps = get_comps(text);
            if(comps !== null){
                const {name, num_cycles} = comps;
                station_map[name.substr(0,5)] = { num_cycles, link };
            }
        });
        //Now search for station in order of preference
        for(let station of stations){
            console.log(`checking for ${station}...`);
            if(!station_map[station]) continue; 
            const {num_cycles, link} = station_map[station];
            if(num_cycles && num_cycles>0){
                return link;
            }
        }
        return null;
    }, get_comps.toString(), stations);
    if(handle._remoteObject.className === 'HTMLAnchorElement' ){
        const promise = page.waitForNavigation({waitUntil: 'domcontentloaded'});
        handle.click();
        await promise;
        return true;
    }
    return false;
}

async function find_station(page){
    return new Promise((resolve, reject)=>{
        const int_id = setInterval(async ()=>{
            await page.reload({waitUntil: 'domcontentloaded'});
            const cur_time = moment().format('MMMM Do YYYY, h:mm:ss a');
            console.log(`Checking for station @ ${cur_time}`);
            await print_stations(page, config.stations);
            const found_station = await check_for_station(page, config.stations);
            if(found_station){
                console.log('Found a station');
                if(config.playSound){
                    player.play('assets/alarm.mp3', function(err){ if (err) throw err; });
                }
                clearInterval(int_id);
                resolve(true);
            }else{
                console.log(`Didn't find any stations. Will try again`);
            }
        }, config.refreshInterval);
    });
}

const connect_chrome = async () => {
    const page = await get_page(null);
    await page.goto(config.url, { waitUntil: 'networkidle2'});
    await page.type('.main_inner01 input[name="MemberID"]', config.credentials.username);
    await page.type('.main_inner01 input[type="password"]', config.credentials.password);

    await wait_navigation_after_action(page, 'click', '.main_inner01 input[type="submit"]', null);
    await wait_navigation_after_action(page, 'click', 'div.main_inner_wide > div:nth-child(6) > div > form:nth-child(3) > div > a', null);
    await wait_navigation_after_action(page, 'select', 'select[name="Location"]', config.location);
    await find_station(page);
    console.log('Reserving the first bicycle available');
    await wait_navigation_after_action(page, 'click', '#cycBtnTab_0'); 
}


connect_chrome();
